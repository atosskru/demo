package com.atoss.recruit.server.security;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class SecurityUtils {

	public static String hashPassword(String password) {
		try {
			return PasswordHash.createHash(password);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Improve
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public static boolean checkPassword(String savedHash, String password) {
		try {
			return PasswordHash.validatePassword(password, savedHash);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Improve
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}
