package com.atoss.recruit.server.security;

public enum UserType {
	ADMIN, 
	EXAMINER, 
	CANDIDATE
}
