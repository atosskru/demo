package com.atoss.recruit.server.security;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
public class PKBEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		return SecurityUtils.hashPassword(rawPassword.toString());
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return SecurityUtils.checkPassword(encodedPassword, rawPassword.toString());
	}

}
