package com.atoss.recruit.server.boot;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atoss.recruit.server.facade.impl.UserService;

@Component
public class DefaultUserCreationBean {

	private @Autowired UserService mUserService;
	
	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		mUserService.createAdminUser("taris", "atoss", "taris@atoss.com");
	}
}
