package com.atoss.recruit.server.db.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atoss.recruit.server.db.entity.User;


public interface UserDao extends JpaRepository<User, Long> {

	Optional<User> findOneByUsername(String username);
	
}
