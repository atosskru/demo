package com.atoss.recruit.server.facade.security.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.atoss.recruit.server.db.entity.User;
import com.atoss.recruit.server.facade.impl.UserService;
import com.atoss.recruit.server.security.RecruitUser;

@Component
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired private UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Optional<User> storedUser = userService.findByUsername(username);
		if (storedUser.isPresent()) {
			return new RecruitUser(storedUser.get());
		}
		return null;
	}

}
