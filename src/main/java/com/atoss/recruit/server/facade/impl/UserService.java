package com.atoss.recruit.server.facade.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atoss.recruit.server.db.dao.UserDao;
import com.atoss.recruit.server.db.entity.User;
import com.atoss.recruit.server.security.SecurityUtils;
import com.atoss.recruit.server.security.UserType;

@Service
public class UserService {

	public class UsernameAlreadyExistsException extends Exception {

		private static final long serialVersionUID = 1L;

	}

	public class InvalidPasswordException extends Exception {

		private static final long serialVersionUID = 1L;
		
	}

	public class UsernameNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

	}

	@Autowired
	private UserDao dao;
	
	public User createAdminUser(String username, String password, String email) throws UsernameAlreadyExistsException {
		Optional<User> storedUser = dao.findOneByUsername(username);
		if (storedUser.isPresent()) {
			throw new UsernameAlreadyExistsException();
		}
		User user = new User();
		user.setUsername(username);
		user.setEmail(email);
		user.setPasswordHash(SecurityUtils.hashPassword(password));
		user.setRole(UserType.ADMIN);
		
		return dao.save(user);
	}
	
	public User createApplicantUser(String username, String password, String email) throws UsernameAlreadyExistsException {
		Optional<User> storedUser = dao.findOneByUsername(username);
		if (storedUser.isPresent()) {
			throw new UsernameAlreadyExistsException();
		}
		User user = new User();
		user.setUsername(username);
		user.setEmail(email);
		user.setPasswordHash(SecurityUtils.hashPassword(password));
		user.setRole(UserType.CANDIDATE);
		
		return dao.save(user);
	}
	
	public Optional<User> findByUsername(String userName) {
		return dao.findOneByUsername(userName);
	}

	public void deletUser(String username) {
		final Optional<User> user = dao.findOneByUsername(username);
		if (user.isPresent()) {
			dao.delete(user.get());
		}
	}
	
}
