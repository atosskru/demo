package com.atoss.recruit.server.presentation.web;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.atoss.recruit.server.db.dao.UserDao;
import com.atoss.recruit.server.db.entity.User;
import com.atoss.recruit.server.facade.impl.UserService;
import com.atoss.recruit.server.facade.impl.UserService.UsernameAlreadyExistsException;

@RestController
public class UsersController {

	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public class EntityNotFoundException extends RuntimeException {

		private static final long serialVersionUID = 1L;

	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public class BadRequest extends RuntimeException {

		private static final long serialVersionUID = 1L;

	}
	
	@ResponseStatus(value = HttpStatus.CONFLICT)
	public class DuplicateEntity extends RuntimeException {

		private static final long serialVersionUID = 1L;

	}
	
	@Autowired
	private UserDao dao;
	
	@Autowired
	private UserService service;
	
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, value = "/api/users")
	public List<User> getUsers() {
		return dao.findAll();
	}
	
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, value = "/api/users/:{username}")
	public User getUser(final @PathVariable String username) {
		Optional<User> user = dao.findOneByUsername(username);
		if (user.isPresent()) {
			return user.get();
		}
		throw new EntityNotFoundException();
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/api/users")
	@ResponseStatus(HttpStatus.CREATED)
	public void createUser(final @RequestBody User user, HttpServletResponse response) {
		try {
			service.createApplicantUser(user.getUsername(), user.getPasswordHash(), user.getEmail());
//			response.setStatus(HttpServletResponse.SC_CREATED);
		} catch (UsernameAlreadyExistsException e) {
			throw new DuplicateEntity();
		} catch (Exception e) {
			throw new BadRequest();
		}
	}
	
	
}
