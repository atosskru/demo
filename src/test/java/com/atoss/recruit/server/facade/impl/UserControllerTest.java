package com.atoss.recruit.server.facade.impl;

import static com.atoss.recruit.test.util.TestUtil.APPLICATION_JSON_UTF8;
import static com.atoss.recruit.test.util.TestUtil.convertObjectToJsonBytes;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//import org.apache.struts.mock.MockHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.atoss.recruit.Application;
import com.atoss.recruit.server.db.entity.User;
import com.atoss.recruit.test.util.security.NoSecurityConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, NoSecurityConfig.class})
@WebAppConfiguration
public class UserControllerTest {

	private MockMvc mvc;
	
	@Autowired private UserService service;

	@Autowired
    private WebApplicationContext wac;
	
	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void testContextLoads() {
		assertNotNull(mvc);
	}
	
	@Test
	public void testCreateUser() throws Exception {
		User user = new User();
		user.setEmail("xd@xd.com");
		user.setUsername("XD");
		user.setPasswordHash("test");
		mvc.perform(MockMvcRequestBuilders.post("/api/users")
				.contentType(APPLICATION_JSON_UTF8)
				.content(convertObjectToJsonBytes(user))
				.accept(APPLICATION_JSON_UTF8))
				.andExpect(status().isCreated())
				.andExpect(content().string(""));
		
		// cleanup
		service.deletUser("XD");
		
	}
	
	@Test
	public void testDuplicateUser() throws Exception {
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.get("api/user/get/XD")).andReturn();
		if (result.getResponse().getStatus() == MockHttpServletResponse.SC_NOT_FOUND) {
			// if no such user, create it
			User user = new User();
			user.setEmail("xd@xd.com");
			user.setUsername("XD");
			user.setPasswordHash("test");
			mvc.perform(MockMvcRequestBuilders.put("/api/user/add")
					.contentType(APPLICATION_JSON_UTF8)
					.content(convertObjectToJsonBytes(user))
					.accept(APPLICATION_JSON_UTF8))
					.andExpect(status().isCreated())
					.andExpect(content().string(""));
		}
		
		User user = new User();
		user.setEmail("xd@xd.com");
		user.setUsername("XD");
		user.setPasswordHash("test");
		mvc.perform(MockMvcRequestBuilders.put("/api/user/add")
				.contentType(APPLICATION_JSON_UTF8)
				.content(convertObjectToJsonBytes(user))
				.accept(APPLICATION_JSON_UTF8))
				.andExpect(status().isConflict());
		
		// cleanup
		service.deletUser("XD");
	}
	
	@Test
	public void testBadRequest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/api/user/add")
				.contentType(APPLICATION_JSON_UTF8)
				.content("bad request")
				.accept(APPLICATION_JSON_UTF8))
				.andExpect(status().isBadRequest());
	}
}
