package com.atoss.recruit.server.facade.impl;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atoss.recruit.Application;
import com.atoss.recruit.server.db.entity.User;
import com.atoss.recruit.server.facade.impl.UserService.UsernameAlreadyExistsException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class UserServiceTest {
	
	@Autowired
	private UserService mService;
	
	@Test
	public void testUserServiceCreate() {
		try {
			mService.createApplicantUser("TestUser", "test", "test@test.com");
			
			User user = mService.findByUsername("TestUser").get();
			
			assertNotNull(user);
			assertThat(user.getUsername(), equalTo("TestUser"));
			assertThat(user.getEmail(), equalTo("test@test.com"));
			 
		} catch (UsernameAlreadyExistsException e) {
			fail();
		} finally {
			mService.deletUser("TestUser");
		}
	}
	
	@Test
	public void testUserServiceCreateFailedDuplicate() {
		try  {
			mService.createApplicantUser("TestUser", "test", "test@test.com");
			
			try {
				mService.createApplicantUser("TestUser", "test", "test@test.com");
				fail();
			} catch (UsernameAlreadyExistsException e) {
				
			}
		} catch (UsernameAlreadyExistsException e) {
			fail(e.getClass().getCanonicalName());
		} finally {
			mService.deletUser("TestUser");
		}
	}
	

}
